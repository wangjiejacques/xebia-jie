//
//  UIViewController+helper.swift
//  xebia-jie
//
//  Created by WANG Jie on 19/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func create<T: UIViewController>(viewControllerClass: T.Type, storyboardName: String? = nil) -> T {
        let className = NSStringFromClass(viewControllerClass).components(separatedBy: ".").last!
        var storyboard = self.storyboard

        if let storyboardName = storyboardName {
            storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        }
        return storyboard?.instantiateViewController(withIdentifier: className) as! T
    }

    static func create<T: UIViewController>(viewControllerClass: T.Type, storyboardName: String) -> T {
        let className = NSStringFromClass(viewControllerClass).components(separatedBy: ".").last!
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: className) as! T
    }
}
