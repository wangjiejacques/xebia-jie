//
//  APIBooks.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

protocol BooksAPI: class {
    func getBooks(successHandler: @escaping ([Book]) -> Void, failureHandler: @escaping (XBError) -> Void)
}

extension APIClient: BooksAPI {
    func getBooks(successHandler: @escaping ([Book]) -> Void, failureHandler: @escaping (XBError) -> Void) {
        doRequest(method: .get, urlPath: "books", successHandler: { (json) in
            let books = createModels(modelType: Book.self, jsons: json.array)
            successHandler(books)
        }) { (error) in
            failureHandler(error)
        }
    }
}


class BooksAPIMock: BooksAPI {
    var getBooksShouldCallSuccessHandler = true
    var getBooksSuccessHandlerParam0: [Book]!
    var getBooksShouldCallFailureHandler = true
    var getBooksFailureHandlerParam0: XBError!

    var getBooksSuccessHandlerWasCalled: Bool?

    func getBooks(successHandler: @escaping ([Book]) -> Void, failureHandler: @escaping (XBError) -> Void) {
        if getBooksShouldCallSuccessHandler {
            successHandler(getBooksSuccessHandlerParam0)
        }
        if getBooksShouldCallFailureHandler {
            failureHandler(getBooksFailureHandlerParam0)
        }
        getBooksSuccessHandlerWasCalled = true
    }

}
