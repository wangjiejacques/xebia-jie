//
//  OffersView.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

protocol OffersView: class {
    func show(bookOrders: [BookOrder])

    func show(bestOffer offer: Offer?, finalPrice price: Double)

    func show(error: XBError)

    func showRefresh()

    func hideRefresh()
}




class OffersViewMock: OffersView {
    var showBookOrders: [BookOrder]?
    var showOffer: Offer?
    var showPrice: Double?
    var showError: XBError?

    var showBookOrdersWasCalled: Bool?
    var showBestOfferWasCalled: Bool?
    var showErrorWasCalled: Bool?
    var showRefreshWasCalled: Bool?
    var hideRefreshWasCalled: Bool?

    func show(bookOrders: [BookOrder]) {
        showBookOrders = bookOrders
        showBookOrdersWasCalled = true
    }

    func show(bestOffer offer: Offer?, finalPrice price: Double) {
        showOffer = offer
        showPrice = price
        showBestOfferWasCalled = true
    }

    func show(error: XBError) {
        showError = error
        showErrorWasCalled = true
    }

    func showRefresh() {
        showRefreshWasCalled = true
    }

    func hideRefresh() {
        hideRefreshWasCalled = true
    }

}
