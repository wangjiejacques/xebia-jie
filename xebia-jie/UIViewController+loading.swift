//
//  UIViewController+loading.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation


import Foundation
import UIKit

let activityTag = 99

extension UIViewController {

    private static var rootView: UIView? {
        return AppDelegate.shared.window?.rootViewController?.view
    }

    // if view is nil, show activity in rootView.
    // don't show two activity indicator in one controller.
    func showLoading(inView view: UIView? = nil) {
        UIViewController.showLoading(inView: view)
    }

    static func showLoading(inView view: UIView? = nil) {
        guard let activityParent = view ?? rootView else { return }
        guard activityParent.viewWithTag(activityTag) == nil else { return }
        let activity = UIActivityIndicatorView()
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        activity.startAnimating()
        activity.tag = activityTag
        activityParent.addSubview(activity)
        let centerXConstraint = NSLayoutConstraint(item: activity, attribute: .centerX, relatedBy: .equal, toItem: activityParent, attribute: .centerX, multiplier: 1, constant: 0)
        let centerYConstraint = NSLayoutConstraint(item: activity, attribute: .centerY, relatedBy: .equal, toItem: activityParent, attribute: .centerY, multiplier: 1, constant: 0)
        let widthEqual = NSLayoutConstraint(item: activity, attribute: .width, relatedBy: .equal, toItem: activityParent, attribute: .width, multiplier: 1, constant: 0)
        let heightEqual = NSLayoutConstraint(item: activity, attribute: .height, relatedBy: .equal, toItem: activityParent, attribute: .height, multiplier: 1, constant: 0)
        activityParent.addConstraints([centerXConstraint, centerYConstraint, widthEqual, heightEqual])
        activity.layoutMargins = UIEdgeInsets.zero
    }

    func hideLoading(inView view: UIView? = nil) {
        UIViewController.hideLoading()
    }

    static func hideLoading(inView view: UIView? = nil) {
        guard let activityParent = view ?? rootView else { return }
        activityParent.viewWithTag(activityTag)?.removeFromSuperview()
    }
}
