//
//  BooksView.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

protocol BooksView: class {
    func show(books: [Book])

    func show(error: XBError)

    func show(booksCount count: Int)

    func showRefresh()

    func hideRefresh()

    func showCart(books: [Book])
}



// generate by CodeGenerator
class BooksViewMock: BooksView {
    var showBooks: [Book]?
    var showError: XBError?
    var showCount: Int?
    var showCartBooks: [Book]?

    var showBooksWasCalled: Bool?
    var showErrorWasCalled: Bool?
    var showBooksCountWasCalled: Bool?
    var showRefreshWasCalled: Bool?
    var hideRefreshWasCalled: Bool?
    var showCartBooksWasCalled: Bool?

    func show(books: [Book]) {
        showBooks = books
        showBooksWasCalled = true
    }

    func show(error: XBError) {
        showError = error
        showErrorWasCalled = true
    }

    func show(booksCount count: Int) {
        showCount = count
        showBooksCountWasCalled = true
    }

    func showRefresh() {
        showRefreshWasCalled = true
    }

    func hideRefresh() {
        hideRefreshWasCalled = true
    }

    func showCart(books: [Book]) {
        showCartBooks = books
        showCartBooksWasCalled = true
    }

}
