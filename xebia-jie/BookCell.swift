//
//  BookCell.swift
//  xebia-jie
//
//  Created by WANG Jie on 19/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import UIKit

protocol BookCellDelegate: class {
    func bookCellDidClickAddBook(_ bookCell: BookCell, _ book: Book)

    func bookCellDidClickMinusBook(_ bookCell: BookCell, _ book: Book)
}

class BookCell: UITableViewCell {



    @IBOutlet weak var bookItemView: BookItemView!

    var book: Book!
    weak var delegate: BookCellDelegate?

    func config(book: Book) {
        self.book = book
        bookItemView.config(book: book)
    }

    @IBAction func didClickAdd(_ sender: Any) {
        delegate?.bookCellDidClickAddBook(self, book)
    }

    @IBAction func didClickMinus(_ sender: Any) {
        delegate?.bookCellDidClickMinusBook(self, book)
    }
}

protocol Cellable {
    static var identifier: String { get }
}

extension Cellable where Self: UITableViewCell {
    static var identifier: String {
        return String(describing: Self.self)
    }
}

extension UITableViewCell: Cellable { }
