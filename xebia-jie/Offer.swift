//
//  Offer.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import SwiftyJSON

class Offer: Modelable {
    let value: Double

    func applyingOffer(to price: Double) -> Double {
        preconditionFailure("Override this method")
    }

    init(value: Double) {
        self.value = value
    }

    required init(json: JSON) {
        self.value = json["value"].double ?? 0
    }

    var description: String {
        preconditionFailure("Override")
    }
}

class PercentageOffer: Offer {
    override func applyingOffer(to price: Double) -> Double {
        return price * (100 - value) / 100
    }

    override var description: String {
        return "\(value) percentage de réduction"
    }
}

class MinusOffer: Offer {
    override func applyingOffer(to price: Double) -> Double {
        return price - value
    }

    override var description: String {
        return "\(value)€ de déduction"
    }
}

class SliceOffer: Offer {
    let sliceValue: Double

    init(value: Double, sliceValue: Double) {
        self.sliceValue = sliceValue
        super.init(value: value)
    }

    required init(json: JSON) {
        self.sliceValue = json["sliceValue"].double ?? 0
        super.init(json: json)
    }

    override func applyingOffer(to price: Double) -> Double {
        guard price >= sliceValue else {
            return price
        }
        return price - value
    }

    override var description: String {
        return "\(value)€ de remboursement par tranche de \(sliceValue)€ d’achat"
    }
}

struct OfferFactory {

    static let offerTypes: [String: Offer.Type] = ["percentage": PercentageOffer.self, "minus": MinusOffer.self, "slice": SliceOffer.self]

    static func createOffers(jsons: [JSON]?) -> [Offer] {
        guard let jsons = jsons else { return [] }
        return jsons.map { json in
            let offerType = offerTypes[json["type"].string!]!
            return offerType.init(json: json)
        }
    }
}
