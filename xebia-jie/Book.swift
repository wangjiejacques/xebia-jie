//
//  Book.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Book: Modelable {
    private (set) var isbn: String
    let title: String
    private (set) var price: Double
    let cover: String
    let synopsis: [String]

    init(json: JSON) {
        isbn = json["isbn"].string ?? ""
        title = json["title"].string ?? ""
        price = json["price"].double ?? 0
        cover = json["cover"].string ?? ""
        synopsis = (json["synopsis"].array ?? []).flatMap { $0.string }
    }

    init(price: Double) {
        self.init()
        self.price = price
        self.isbn = UUID().uuidString
    }
}

extension Double {
    var humanReadable: String {
        return String(format: "%.2f", self)
    }
}
