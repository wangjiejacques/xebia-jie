//
//  BookItemView.swift
//  xebia-jie
//
//  Created by WANG Jie on 20/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import UIKit

class BookItemView: UIView {

    @IBOutlet weak var bookImageView: UIImageView!
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var bookPriceLabel: UILabel!
    @IBOutlet weak var bookSynopsisLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        let view = Bundle.main.loadNibNamed("BookItemView", owner: self, options: nil)?.first as! UIView
        addSubview(view)
        view.frame = bounds
    }

    func config(book: Book) {
        bookImageView.setImage(with: book.cover)
        bookTitleLabel.text = book.title
        bookPriceLabel.text = "EUR \(book.price.humanReadable)"
        bookSynopsisLabel.text = book.synopsis[0]
    }
}
