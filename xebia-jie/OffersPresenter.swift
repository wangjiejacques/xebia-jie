//
//  OffersPresenter.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

class OffersPresenter {
    weak var offersView: OffersView!
    var offerAPI: OfferAPI
    var books: [Book]

    init(offersView: OffersView, offerAPI: OfferAPI, books: [Book]) {
        self.offersView = offersView
        self.offerAPI = offerAPI
        self.books = books
    }

    func presentOffres() {
        let bookOrders = computedBookOrders
        offersView.show(bookOrders: bookOrders)
        guard !bookOrders.isEmpty else {
            offersView.show(bestOffer: nil, finalPrice: 0)
            return
        }
        offersView.showRefresh()
        offerAPI.getOffers(books: books, successHandler: { (offers) in
            self.offersView.hideRefresh()
            let totalPrice = bookOrders.reduce(0, { $0 + $1.price })
            let sortedOffers = offers.sorted(by: { $0.applyingOffer(to: totalPrice) < $1.applyingOffer(to: totalPrice) })
            let bestOffer = sortedOffers.first
            self.offersView.show(bestOffer: bestOffer, finalPrice: bestOffer?.applyingOffer(to: totalPrice) ?? totalPrice)
        }) { (error) in
            self.offersView.show(error: error)
            self.offersView.hideRefresh()
        }
    }

    private var computedBookOrders: [BookOrder] {
        var bookOrders = [BookOrder]()
        books.forEach { book in
            if let orderIndex = bookOrders.index(where: { book.isbn == $0.book.isbn }) {
                let existOrder = bookOrders[orderIndex]
                existOrder.quantity += 1
            } else {
                let newBookOrder = BookOrder(book: book, quantity: 1)
                bookOrders.append(newBookOrder)
            }
        }
        return bookOrders
    }
}
