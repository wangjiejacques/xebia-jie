//
//  BooksViewController.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import UIKit
import KissToast

class BooksViewController: UIViewController {

    var presenter: BooksPresenter!
    fileprivate var books: [Book] = []

    @IBOutlet weak var tableView: UITableView!
    var cartButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Books"

        presenter = BooksPresenter(booksView: self, booksAPI: APIClient.shared)
        presenter.getBooks()

        tableView.dataSource = self
        tableView.rowHeight = 150

        cartButton = UIBarButtonItem(title: "Cart", style: .done, target: presenter, action: #selector(BooksPresenter.done))
        navigationItem.rightBarButtonItem = cartButton
    }
}

extension BooksViewController: BookCellDelegate {
    func bookCellDidClickAddBook(_ bookCell: BookCell, _ book: Book) {
        presenter.add(book: book)
    }

    func bookCellDidClickMinusBook(_ bookCell: BookCell, _ book: Book) {
        presenter.minus(book: book)
    }
}

extension BooksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.identifier, for: indexPath) as! BookCell
        let book = books[indexPath.row]
        cell.config(book: book)
        cell.delegate = self
        return cell
    }
}

extension BooksViewController: BooksView {
    func show(books: [Book]) {
        self.books = books
        tableView.reloadData()
    }

    func show(error: XBError) {
        Toast.Builder(text: error.message).bottomSpace(space: 150).build().show()
    }

    func show(booksCount count: Int) {
        cartButton.title = "Cart(\(count))"
    }

    func showRefresh() {
        showLoading()
    }

    func hideRefresh() {
        hideLoading()
    }

    func showCart(books: [Book]) {
        let offersViewController = OffersViewController.create(books)
        navigationController!.pushViewController(offersViewController, animated: true)
    }
}
