//
//  CartViewController.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import UIKit
import KissToast

class OffersViewController: UIViewController {

    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: OffersPresenter!
    fileprivate var bookOrders: [BookOrder] = []
    
    static func create(_ books: [Book]) -> OffersViewController {
        let vc = UIViewController.create(viewControllerClass: self, storyboardName: "Main")
        vc.presenter = OffersPresenter(offersView: vc, offerAPI: APIClient.shared, books: books)
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cart"
        tableView.dataSource = self
        tableView.rowHeight = 190

        presenter.presentOffres()
    }
}

extension OffersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookOrders.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookOrderCell.identifier, for: indexPath) as! BookOrderCell
        let bookOrder = bookOrders[indexPath.row]
        cell.config(bookOrder: bookOrder)
        return cell
    }
}

extension OffersViewController: OffersView {
    func show(bookOrders: [BookOrder]) {
        let totalQuantity = bookOrders.reduce(0, { $0 + $1.quantity })
        quantityLabel.text = "Subtotal (\(totalQuantity) books):"
        self.bookOrders = bookOrders
        tableView.reloadData()
    }

    func show(bestOffer offer: Offer?, finalPrice price: Double) {
        priceLabel.text = "EUR \(price.humanReadable)"
        offerLabel.text = offer?.description
    }

    func show(error: XBError) {
        Toast.Builder(text: error.message).bottomSpace(space: 150).build().show()
    }

    func showRefresh() {
        showLoading()
    }

    func hideRefresh() {
        hideLoading()
    }
}
