//
//  UIImageView+image.swift
//  xebia-jie
//
//  Created by WANG Jie on 19/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {

    func setImage(with URLString: String?, placeholder: Image? = nil) {
        guard let URLString = URLString, let URL = URL(string: URLString) else {
            image = placeholder
            return
        }

        kf.setImage(with: URL, placeholder: placeholder)

    }
    
}
