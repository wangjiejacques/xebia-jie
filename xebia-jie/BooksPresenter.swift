//
//  BooksPresenter.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

class BooksPresenter: NSObject {
    weak var booksView: BooksView?
    var booksAPI: BooksAPI
    var addedBooks: [Book]

    init(booksView: BooksView, booksAPI: BooksAPI) {
        self.booksView = booksView
        self.booksAPI = booksAPI
        addedBooks = []
    }

    func getBooks() {
        booksView?.showRefresh()
        booksAPI.getBooks(successHandler: { (books) in
            self.booksView?.show(books: books)
            self.booksView?.hideRefresh()
        }) { (error) in
            self.booksView?.show(error: error)
            self.booksView?.hideRefresh()
        }
    }

    func add(book: Book) {
        addedBooks.append(book)
        booksView?.show(booksCount: addedBooks.count)
    }

    func minus(book: Book) {
        guard let index = addedBooks.index(where: { $0.isbn == book.isbn }) else {
            return
        }
        addedBooks.remove(at: index)
        booksView?.show(booksCount: addedBooks.count)
    }

    func done() {
        booksView?.showCart(books: addedBooks)
    }
}
