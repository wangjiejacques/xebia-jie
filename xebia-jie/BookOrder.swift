//
//  BookOrder.swift
//  xebia-jie
//
//  Created by WANG Jie on 19/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

class BookOrder {
    let book: Book
    var quantity: Int

    var price: Double {
        return book.price * Double(quantity)
    }

    init(book: Book, quantity: Int) {
        self.book = book
        self.quantity = quantity
    }
}
