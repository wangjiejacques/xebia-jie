//
//  OfferAPI.swift
//  xebia-jie
//
//  Created by WANG Jie on 19/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation

protocol OfferAPI {
    func getOffers(books: [Book], successHandler: @escaping ([Offer]) -> Void, failureHandler: @escaping (XBError) -> Void)
}

extension APIClient: OfferAPI {
    func getOffers(books: [Book], successHandler: @escaping ([Offer]) -> Void, failureHandler: @escaping (XBError) -> Void) {
        let isbns = books.map { $0.isbn }.joined(separator: ",")
        doRequest(method: .get, urlPath: "books/\(isbns)/commercialOffers", successHandler: { (json) in
            let offers = OfferFactory.createOffers(jsons: json["offers"].array)
            successHandler(offers)
        }) { (error) in
            failureHandler(error)
        }
    }
}


class OfferAPIMock: OfferAPI {
    var getOffersBooks: [Book]?
    var getOffersShouldCallSuccessHandler = true
    var getOffersSuccessHandlerParam0: [Offer]!
    var getOffersShouldCallFailureHandler = true
    var getOffersFailureHandlerParam0: XBError!

    var getOffersBooksWasCalled: Bool?

    func getOffers(books: [Book], successHandler: @escaping ([Offer]) -> Void, failureHandler: @escaping (XBError) -> Void) {
        getOffersBooks = books
        if getOffersShouldCallSuccessHandler {
            successHandler(getOffersSuccessHandlerParam0)
        }
        if getOffersShouldCallFailureHandler {
            failureHandler(getOffersFailureHandlerParam0)
        }
        getOffersBooksWasCalled = true
    }

}
