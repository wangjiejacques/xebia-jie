//
//  Modelable.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol Modelable {
    init(json: JSON)

    init()
}

extension Modelable {
    init() {
        self.init(json: JSON(parseJSON: ""))
    }
}

func createModels<M: Modelable>(modelType: M.Type, jsons: [JSON]?) -> [M] {
    return (jsons ?? []).map { M.init(json: $0) }
}
