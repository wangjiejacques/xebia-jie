//
//  BookOrderCell.swift
//  xebia-jie
//
//  Created by WANG Jie on 20/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import UIKit

class BookOrderCell: UITableViewCell {

    @IBOutlet weak var bookItemView: BookItemView!
    @IBOutlet weak var quantityLabel: UILabel!

    var bookOrder: BookOrder!

    func config(bookOrder: BookOrder) {
        self.bookOrder = bookOrder
        bookItemView.config(book: bookOrder.book)
        quantityLabel.text = "quantity: \(bookOrder.quantity)"
    }
}
