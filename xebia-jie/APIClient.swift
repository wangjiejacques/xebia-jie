//
//  APIClient.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias api = APIClient

struct XBError {
    let code: Int
    let message: String
    static let `default` = XBError(code: 600, message: "An Error Occured!")
}

class APIClient {
    var sessionManager: Alamofire.SessionManager!

    static var shared: APIClient = {
        let apiClient = APIClient()
        apiClient.sessionManager = SessionManager.default
        return apiClient
    }()

    var headers: [String: String] {
        var headers = [String: String]()
        headers["Content-Type"] = "application/json"
        return headers
    }

    @discardableResult func doRequest(method: Alamofire.HTTPMethod, urlPath: String,  parameters: [String : Any]? = nil, successHandler: @escaping (JSON) -> Void, failureHandler: @escaping (XBError) -> Void) -> Request {

        let encoding: ParameterEncoding = method == .get ? Alamofire.URLEncoding.default : Alamofire.JSONEncoding.default

        let request = sessionManager.request(APIClient.baseURL + urlPath, method: method, parameters: parameters, encoding: encoding, headers: headers)

        request.validate().responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successHandler(json)
            case .failure(let error):
                guard let error = error as? AFError, let code = error.responseCode, let message = error.errorDescription else {
                    failureHandler(.default)
                    return
                }
                failureHandler(XBError(code: code, message: message))
            }
        }
        return request
    }

}

extension APIClient {
    static let baseURL = "http://henri-potier.xebia.fr/"
}
