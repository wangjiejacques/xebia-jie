//
//  OfferSpec.swift
//  xebia-jie
//
//  Created by WANG Jie on 20/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import xebia_jie

class OfferSpec: QuickSpec {

    override func spec() {
        var offerPresenter: OffersPresenter!
        var offerAPI: OfferAPIMock!
        var offerView: OffersViewMock!

        beforeEach {
            offerAPI = OfferAPIMock()
            offerView = OffersViewMock()
            offerPresenter = OffersPresenter(offersView: offerView, offerAPI: offerAPI, books: [])
        }

        describe("Given a user see see the offre") {
            context("When the offres are fetched successfully") {
                let minusOffer = MinusOffer(value: 15)
                beforeEach {
                    offerAPI.getOffersShouldCallSuccessHandler = true
                    offerAPI.getOffersShouldCallFailureHandler = false
                    let book0 = Book(price: 35)
                    let book1 = Book(price: 30)
                    offerPresenter.books = [book0, book1]
                }
                context("When there are no books selected") {
                    beforeEach {
                        offerPresenter.books = []
                        offerAPI.getOffersShouldCallFailureHandler = false
                        offerAPI.getOffersShouldCallSuccessHandler = false
                        offerPresenter.presentOffres()
                    }
                    it("Then the view should not fetch the offer") {
                        expect(offerView.showRefreshWasCalled).to(beNil())
                        expect(offerAPI.getOffersBooksWasCalled).to(beNil())
                    }
                    it("Then the view shows the price as 0") {
                        expect(offerView.showPrice).to(equal(0))
                    }
                }
                context("When the user get 3 offres") {
                    beforeEach {
                        let percentageOffer = PercentageOffer(value: 5)
                        let sliceOffer = SliceOffer(value: 12, sliceValue: 100)
                        offerAPI.getOffersSuccessHandlerParam0 = [percentageOffer, minusOffer, sliceOffer]
                        offerPresenter.presentOffres()
                    }
                    it("Then the view shows refresh") {
                        expect(offerView.showRefreshWasCalled).to(beTrue())
                    }
                    it("Then the view shows books order") {
                        expect(offerView.showBookOrdersWasCalled).to(beTrue())
                    }
                    it("Then the view shows the minusOffer as the best offer") {
                        expect(offerView.showOffer?.value).to(equal(minusOffer.value))
                    }
                    it("Then the view hides refresh") {
                        expect(offerView.hideRefreshWasCalled).to(beTrue())
                    }
                    it("Then the view doesn't show the error message") {
                        expect(offerView.showErrorWasCalled).to(beNil())
                    }
                }
                context("When the user get 0 offres") {
                    beforeEach {
                        offerAPI.getOffersSuccessHandlerParam0 = []
                        offerPresenter.presentOffres()
                    }
                    it("Then the view shows the non best best offer") {
                        expect(offerView.showOffer).to(beNil())
                        expect(offerView.showBestOfferWasCalled).to(beTrue())
                    }

                }
                context("When the user get 1 offer") {
                    beforeEach {
                        offerAPI.getOffersSuccessHandlerParam0 = [minusOffer]
                        offerPresenter.presentOffres()
                    }
                    it("Then the view shows the minusOffer as the best offer") {
                        expect(offerView.showOffer).to(beIdenticalTo(minusOffer))
                    }
                }
            }
            context("When the offers aren't fetched successfully") {
                beforeEach {
                    offerAPI.getOffersShouldCallSuccessHandler = false
                    offerAPI.getOffersShouldCallFailureHandler = true
                    let book0 = Book(price: 35)
                    let book1 = Book(price: 30)
                    offerPresenter.books = [book0, book1]
                    offerAPI.getOffersFailureHandlerParam0 = XBError.default
                    offerPresenter.presentOffres()
                }
                it("Then the view shows the books view") {
                    expect(offerView.showBookOrdersWasCalled).to(beTrue())
                }
                it("Then the view shows the refresh") {
                    expect(offerView.showRefreshWasCalled).to(beTrue())
                }
                it("Then the view doesn't show the offer") {
                    expect(offerView.showBestOfferWasCalled).to(beNil())
                }
                it("Then the view shows the error") {
                    expect(offerView.showErrorWasCalled).to(beTrue())
                }
                it("Then the view hides the refresh") {
                    expect(offerView.hideRefreshWasCalled).to(beTrue())
                }
            }

        }
    }
    
}
