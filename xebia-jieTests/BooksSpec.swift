//
//  BooksSpec.swift
//  xebia-jie
//
//  Created by WANG Jie on 18/02/2017.
//  Copyright © 2017 wangjie. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import xebia_jie

class BooksSpec: QuickSpec {

    override func spec() {
        var booksView: BooksViewMock!
        var booksAPI: BooksAPIMock!
        var booksPresenter: BooksPresenter!

        beforeEach {
            booksView = BooksViewMock()
            booksAPI = BooksAPIMock()
            booksAPI.getBooksFailureHandlerParam0 = XBError.default
            booksAPI.getBooksSuccessHandlerParam0 = []
            booksPresenter = BooksPresenter(booksView: booksView, booksAPI: booksAPI)
        }
        describe("Given a user see the books view") {
            context("When the books were fetched successfully") {
                beforeEach {
                    booksAPI.getBooksShouldCallSuccessHandler = true
                    booksAPI.getBooksShouldCallFailureHandler = false
                    booksPresenter.getBooks()
                }
                it("Then the books view shows refresh") {
                    expect(booksView.showRefreshWasCalled).to(beTrue())
                }
                it("Then the books view shows the book") {
                    expect(booksView.showBooksWasCalled).to(beTrue())
                }
                it("Then the books view doesn't show the error") {
                    expect(booksView.showErrorWasCalled).to(beNil())
                }
                it("Then the books view hides the refresh") {
                    expect(booksView.hideRefreshWasCalled).to(beTrue())
                }
            }
            context("When the books were fetched unsuccessfully") {
                beforeEach {
                    booksAPI.getBooksShouldCallSuccessHandler = false
                    booksAPI.getBooksShouldCallFailureHandler = true
                    booksPresenter.getBooks()
                }
                it("Then the books view show refresh") {
                    expect(booksView.showRefreshWasCalled).to(beTrue())
                }
                it("Then the books view doesn't show the book") {
                    expect(booksView.showBooksWasCalled).to(beNil())
                }
                it("Then the books view shows the error") {
                    expect(booksView.showErrorWasCalled).to(beTrue())
                }
                it("Then the books view hides the refresh") {
                    expect(booksView.hideRefreshWasCalled).to(beTrue())
                }
            }
        }
        describe("Given a user add a book") {
            beforeEach {
                booksPresenter.add(book: Book())
            }
            it("Then the books view shows books count as 1") {
                expect(booksView.showCount).to(equal(1))
            }
            context("When the user minus a book") {
                beforeEach {
                    booksPresenter.minus(book: Book())
                }
                it("Then the books view shows books count as 0") {
                    expect(booksView.showCount).to(equal(0))
                }
            }
        }

        describe("Given a user click the cart button") {
            beforeEach {
                booksPresenter.done()
            }
            it("Then the books view shows the cart view") {
                expect(booksView.showCartBooksWasCalled).to(beTrue())
            }
        }
    }
}
